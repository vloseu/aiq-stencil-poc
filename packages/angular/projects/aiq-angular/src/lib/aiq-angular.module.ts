import { NgModule } from '@angular/core';
import { defineCustomElements } from 'aiq-core/loader';
import { AiqBadge } from './directives/proxies';

defineCustomElements(window);

// The "PROXIES" should be always updated when new component is added to "aiq-core" lib
const PROXIES = [
  AiqBadge
];

@NgModule({
  declarations: PROXIES,
  exports: PROXIES,
  imports: [],
  providers: []
})
export class AiqAngularModule { }
