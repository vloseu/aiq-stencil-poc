/*
 * Public API Surface of aiq-angular
 */
export * from './lib/directives/proxies';
export * from './lib/aiq-angular.module';
