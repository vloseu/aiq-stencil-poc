/* eslint-disable */
/* tslint:disable */
/* auto-generated react proxies */
import { createReactComponent } from './react-component-lib';

import type { JSX } from 'aiq-core';



export const AiqBadge = /*@__PURE__*/createReactComponent<JSX.AiqBadge, HTMLAiqBadgeElement>('aiq-badge');
