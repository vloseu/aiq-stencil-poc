import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AiqAngularModule } from 'aiq-angular';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AiqAngularModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
