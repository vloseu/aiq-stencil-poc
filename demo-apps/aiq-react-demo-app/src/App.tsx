import React from 'react';
import logo from './logo.svg';
import './App.css';

import { AiqBadge } from 'aiq-react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Testing AiqBadge component from aiq-react lib - <AiqBadge size="medium" color="success">5</AiqBadge>
        </p>
      </header>
    </div>
  );
}

export default App;
