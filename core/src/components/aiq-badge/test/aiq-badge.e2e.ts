import { newE2EPage } from '@stencil/core/testing';

describe('aiq-badge', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<aiq-badge></aiq-badge>');

    const element = await page.find('aiq-badge');
    expect(element).toHaveClass('hydrated');
  });
});
