import { newSpecPage } from '@stencil/core/testing';
import { AiqBadge } from '../aiq-badge';

describe('aiq-badge', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [AiqBadge],
      html: `<aiq-badge></aiq-badge>`,
    });
    expect(page.root).toEqualHtml(`
      <aiq-badge>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </aiq-badge>
    `);
  });
});
