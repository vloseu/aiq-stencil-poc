import { Component, Host, h, Prop } from '@stencil/core';
import classNames from "classnames";

export type BadgeColor = 'default' | 'info' | 'success' | 'warning' | 'danger';
export type BadgeSize = 'large' | 'medium' | 'small' ;
export type BadgeWeight = 'light' | 'heavy';

@Component({
  tag: 'aiq-badge',
  styleUrl: 'aiq-badge.scss',
  scoped: true
})
export class AiqBadge {
  @Prop() color: BadgeColor = 'default';
  @Prop() size: BadgeSize = 'medium';
  @Prop() weight: BadgeWeight = 'light';
  @Prop() inline: boolean = true;

  render() {
    return (
      <Host class={classNames(this.color, this.size, this.weight, this.inline && 'inline' )}>
        <slot></slot>
      </Host>
    );
  }

}
