# aiq-badge



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                                                        | Default     |
| -------- | --------- | ----------- | ----------------------------------------------------------- | ----------- |
| `color`  | `color`   |             | `"danger" \| "default" \| "info" \| "success" \| "warning"` | `'default'` |
| `inline` | `inline`  |             | `boolean`                                                   | `true`      |
| `size`   | `size`    |             | `"large" \| "medium" \| "small"`                            | `'medium'`  |
| `weight` | `weight`  |             | `"heavy" \| "light"`                                        | `'light'`   |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
